const {MongoClient, ObjectID} = require('mongodb');

MongoClient.connect('mongodb://localhost:27017/TodoApp', (err, db) => {
    if(err) {
        return console.log('Unable to connect to mongo db server');
    }
    console.log('Connected to local MongoDB server');

    // db.collection('users').find().toArray().then((results) => {
    //     console.log(JSON.stringify(results, undefined, 2));
    // }, (err) => { 
    //     console.log('Unable to fetch users');
    // });

    db.collection('users').find({age: 29}).toArray().then((docs) => {
        console.log(docs);
    }, (err) => {
        console.log('unable to fetch user', err)
    });
db.close();
});
