const mongoClient = require('mongodb').MongoClient;

mongoClient.connect('mongodb://localhost:27017/TodoApp', (err, db) => {
    if(err) {
        return console.log('Unable to connect to mongo db server');
    }
    console.log('Connected to local MongoDB server');

    //Create a new collection and add some data

    // db.collection('Todo').insertOne({
    //     text: 'Something i have to do',
    //     completed: false
    // }, (err, result) => {
    //     if(err) {
    //         return console.log('unable to insert todo', err);
    //     }
    //     console.log(JSON.stringify(result.ops, undefined, 2));
    // });

    db.collection('users').insertOne({
        name: 'Andrew',
        age:25,
        sex: 'male',
        username: 'jtanye',
        password:''
    }, (err, result) => {
        if(err) {
            return console.log('Unable to save user', err);
        }

        console.log(JSON.stringify(result.ops, undefined, 2));
    });

db.close();
});
